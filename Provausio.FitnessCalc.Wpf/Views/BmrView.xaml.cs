﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Provausio.FitnessCalc.Core;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Wpf.Views
{
    /// <summary>
    /// Interaction logic for BmrView.xaml
    /// </summary>
    public partial class BmrView : Page
    {
        private readonly BmrViewViewModel _result;

        public BmrView()
        {
            InitializeComponent();
            
            _result = new BmrViewViewModel();
            _result.Request = new BmrRequest();
            DataContext = _result;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var calc = CalculatorFactory.GetCalculator(_result.Request);
            var result = calc.Run();
            _result.Result = string.Format("Your BMR is {0} ({1})", result.GetResult(), result.Description);
        }
    }

    public class BmrViewViewModel : INotifyPropertyChanged
    {
        public BmrRequest Request { get; set; }

        private string _result;

        public string Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Result"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
