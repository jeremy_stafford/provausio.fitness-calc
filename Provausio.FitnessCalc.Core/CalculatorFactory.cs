﻿using Provausio.FitnessCalc.Core.Calculators;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core
{
    public static class CalculatorFactory
    {
        public static IFitnessCalculator GetCalculator(ICalcRequest request)
        {
            // Add new calcs with their request contracts here
            var calcs = new FactorySwitch<ICalcRequest, IFitnessCalculator>()
                .Case(typeof(OneRepMaxCalculator), (req) => new OneRepMaxCalculator(req))
                .Case(typeof(BmiRequest), (req) => new BmiCalculator(req))
                .Case(typeof(BmrRequest), (req) => new BmrCalculator(req))
                .Case(typeof(BaiRequest), (req) => new BaiCalculator(req))
                .Case(typeof(CaloriesBurnedRequest), (req) => new CaloriesBurnedCalculator(req))
                .Case(typeof(MaxHeartRateRequest), (req) => new MaxHeartRateCalculator(req));

            return calcs.Switch(request);
        }
    }
}
