﻿namespace Provausio.FitnessCalc.Core
{
    public enum OperationTypes
    {
        OneRepMax,
        LeanMuscleMass,
        CaloriesBurned,
        BMR,
        BMI,
        BAI,
        MaxHeartRate
    }
}