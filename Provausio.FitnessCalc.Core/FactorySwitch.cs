﻿using System;
using System.Collections.Generic;

namespace Provausio.FitnessCalc.Core
{
    /// <summary>
    /// Generic class assists in the instantiation of a type based on its request type. This class assumes that the request is required in the constructor call.
    /// </summary>
    /// <typeparam name="TInput"></typeparam>
    /// <typeparam name="TOutput"></typeparam>
    public sealed class FactorySwitch<TInput, TOutput>
        where TInput : class
        where TOutput : class
    {
        private readonly Dictionary<Type, Func<TInput, TOutput>> _matches;

        /// <summary>
        /// Default c'tor
        /// </summary>
        public FactorySwitch()
        {
            _matches = new Dictionary<Type, Func<TInput, TOutput>>();
        }

        /// <summary>
        /// Adds a function that will call a parameterized constructor call based on a request type 
        /// </summary>
        /// <param name="type">Request type</param>
        /// <param name="getInstance">Function that will call the constructor using the request</param>
        /// <returns></returns>
        public FactorySwitch<TInput, TOutput> Case(Type type, Func<TInput, TOutput> getInstance)
        {
            _matches.Add(type, getInstance);
            return this;
        }

        /// <summary>
        /// Performs the switch based on the provided request and returns an instance of the appropriate object
        /// </summary>
        /// <param name="request">Instance of the request object that will be used to find the appropriate object or operation</param>
        /// <returns></returns>
        public TOutput Switch(TInput request)
        {
            var type = request.GetType();
            TOutput calculator = null;

            if (_matches.ContainsKey(type))
            {
                calculator = _matches[type](request);
            }

            return calculator;
        }

    }
}