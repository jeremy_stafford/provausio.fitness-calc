﻿using System;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public class CaloriesBurnedCalculator : IFitnessCalculator
    {
        private readonly CaloriesBurnedRequest _request;
        private readonly double _heartRateMultiplier;
        private readonly double _bodyWeightMultiplier;
        private readonly double _ageMultiplier;
        private readonly double _offset;

        public CaloriesBurnedCalculator(ICalcRequest request)
        {
            _request = request as CaloriesBurnedRequest;

            if (_request == null)
            {
                throw new ArgumentException(string.Format("The request {0} was invalid for the operation {1}", request.GetType(), this));
            }

            if (_request.Gender == Gender.Male)
            {
                _heartRateMultiplier = 0.6309;
                _bodyWeightMultiplier = 0.09036;
                _ageMultiplier = 0.2017;
                _offset = 55.0969;
            }
            else
            {
                _heartRateMultiplier = 0.4472;
                _bodyWeightMultiplier = 0.05741;
                _ageMultiplier = 0.074;
                _offset = 20.4022;
            }
        }

        public ICalcResponse Run()
        {
            var heart = (_heartRateMultiplier*_request.AverageHeartRate);
            var weight = (_bodyWeightMultiplier*_request.Weight);
            var age = (_ageMultiplier*_request.Age);

            var result = ((heart - weight + age - _offset)*_request.Minutes)/4.184;
            return new CaloriesBurnedResponse(result);
        }
    }
}
