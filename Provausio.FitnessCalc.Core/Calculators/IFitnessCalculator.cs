﻿using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public interface IFitnessCalculator
    {
        ICalcResponse Run();
    }
}