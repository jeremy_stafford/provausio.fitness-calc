﻿using System;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public class BmrCalculator : IFitnessCalculator
    {
        private readonly BmrRequest _request;
        private readonly double _offset;
        private readonly double _weightMultiplier;
        private readonly double _heightMultiplier;
        private readonly double _ageMultiplier;

        public BmrCalculator(ICalcRequest request)
        {
            _request = request as BmrRequest;

            if (_request == null)
            {
                throw new ArgumentException(string.Format("The request {0} was invalid for the operation {1}", request.GetType(), this));
            }

            if (_request.Gender == Gender.Male)
            {
                _offset = 66;
                _weightMultiplier = 6.23;
                _heightMultiplier = 12.7;
                _ageMultiplier = 6.8;
            }
            else
            {
                _offset = 655;
                _weightMultiplier = 4.35;
                _heightMultiplier = 4.7;
                _ageMultiplier = 4.7;
            }
        }

        public ICalcResponse Run()
        {
            var result = _offset + (_weightMultiplier*_request.Weight) + (_heightMultiplier*_request.Height) -
                         (_ageMultiplier*_request.Age);
            return new BmrResponse(result);
        }
    }
}
