﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class OneRepMaxResponse : CalculatorResponseBase
    {
        public OneRepMaxResponse(double result)
        {
            Operation = OperationTypes.OneRepMax;
            Result = result;
            Description = "Recommended weight for a single max out rep";
        }
    }
}