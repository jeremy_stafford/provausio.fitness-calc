﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class BmrResponse : CalculatorResponseBase
    {
        public BmrResponse(double result)
        {
            Result = result;
            Operation = OperationTypes.BMR;
            Description = "Basal Metabolic Rate";
        }
    }
}