﻿using System.Security.Cryptography;

namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class CaloriesBurnedResponse : CalculatorResponseBase
    {
        public CaloriesBurnedResponse(double result)
        {
            Result = result;
            Operation = OperationTypes.CaloriesBurned;
            Description = "Approximate calories burned";
        }
    }
}