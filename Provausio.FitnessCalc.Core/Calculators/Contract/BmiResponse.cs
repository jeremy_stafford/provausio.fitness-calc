﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class BmiResponse : CalculatorResponseBase
    {
        public BmiResponse(double result)
        {
            Result = result;
            Operation = OperationTypes.BMI;
            Description = "Body Mass Index";
        }
    }
}