﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public abstract class CalculatorResponseBase : ICalcResponse
    {
        protected double Result;
        public OperationTypes Operation { get; protected set; }

        public virtual double GetResult()
        {
            return Result;
        }

        public string Description { get; protected set; }
    }
}