﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class CaloriesBurnedRequest : CalculatorBodyRequestBase
    {

        public CaloriesBurnedRequest()
        {
            Operation = OperationTypes.CaloriesBurned;
        }

        public double AverageHeartRate { get; set; }
        public int Minutes { get; set; }
    }
}