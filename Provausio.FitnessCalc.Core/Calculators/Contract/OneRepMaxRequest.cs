﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class OneRepMaxRequest : ICalcRequest
    {
        /// <summary>
        /// Weight of the work set
        /// </summary>
        public int Weight { get; set; }
        
        /// <summary>
        /// Number of reps that was accomplished successfully
        /// </summary>
        public int Reps { get; set; }

        public OperationTypes Operation
        {
            get { return OperationTypes.OneRepMax; }
        }
    }
}