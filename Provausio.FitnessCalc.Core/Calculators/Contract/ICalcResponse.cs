﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public interface ICalcResponse
    {
        OperationTypes Operation { get; }

        double GetResult();

        string Description { get; }
    }
}