﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public interface ICalcRequest
    {
        OperationTypes Operation { get; }
    }
}