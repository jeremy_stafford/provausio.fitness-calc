﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public class BmiRequest : CalculatorBodyRequestBase
    {
        public BmiRequest()
        {
            Operation = OperationTypes.BMI;
        }
    }
}