﻿namespace Provausio.FitnessCalc.Core.Calculators.Contract
{
    public abstract class CalculatorBodyRequestBase : ICalcRequest
    {
        /// <summary>
        /// Weight in pounds
        /// </summary>
        public double Weight { get; set; }
        /// <summary>
        /// Height in inches
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public int Age { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// Name of the operation
        /// </summary>
        public OperationTypes Operation { get; set; }
        
    }
}