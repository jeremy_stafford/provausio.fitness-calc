﻿using System;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public class OneRepMaxCalculator : IFitnessCalculator
    {
        private readonly OneRepMaxRequest _request;
        public OneRepMaxCalculator(ICalcRequest request)
        {
            _request = request as OneRepMaxRequest;
            if (_request == null)
            {
                throw new ArgumentException(string.Format("The request {0} was invalid for the operation {1}", request.GetType(), this));
            }
        }
        public ICalcResponse Run()
        {
            var rm1 = _request.Weight / (1.0278 - (.0278 * _request.Reps));
            var result = new OneRepMaxResponse((int) rm1);
            return result;
        }
    }
}
