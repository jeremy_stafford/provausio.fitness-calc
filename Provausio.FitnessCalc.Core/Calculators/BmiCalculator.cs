﻿using System;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public class BmiCalculator : IFitnessCalculator
    {
        private readonly BmiRequest _request;

        public BmiCalculator(ICalcRequest request)
        {
            _request = request as BmiRequest;
            if (_request == null)
            {
                throw new ArgumentException(string.Format("The request {0} was invalid for the operation {1}", request.GetType(), this));
            }
        }

        public ICalcResponse Run()
        {
            var result = (_request.Weight/_request.Height)*703;
            return new BmiResponse(result);
        }
    }
}
