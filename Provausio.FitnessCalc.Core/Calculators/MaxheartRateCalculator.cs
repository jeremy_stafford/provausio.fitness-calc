﻿using System;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public class MaxHeartRateCalculator : IFitnessCalculator
    {
        private readonly MaxHeartRateRequest _request;
        private readonly double _ageMultiplier;
        private readonly int _offset;

        public MaxHeartRateCalculator(ICalcRequest request)
        {
            _request = request as MaxHeartRateRequest;
            if (_request == null)
            {
                throw new ArgumentException(string.Format("The request {0} was invalid for the operation {1}", request.GetType(), this));
            }

            if (_request.Gender == Gender.Male)
            {
                _offset = 202;
                _ageMultiplier = 0.55;
            }
            else
            {
                _offset = 216;
                _ageMultiplier = 1.09;
            }
        }
        public ICalcResponse Run()
        {
            var result = _offset - (_ageMultiplier*_request.Age);
            return new MaxHeartRateResponse(result);
        }
    }

    public class MaxHeartRateRequest : CalculatorBodyRequestBase    
    {
        public MaxHeartRateRequest()
        {
            Operation = OperationTypes.MaxHeartRate;
        }
    }

    public class MaxHeartRateResponse : CalculatorResponseBase
    {
        public MaxHeartRateResponse(double result)
        {
            Result = result;
            Operation = OperationTypes.MaxHeartRate;
            Description = "100% of the recommended heart BPM";
        }
    }
}
