﻿using System;
using Provausio.FitnessCalc.Core.Calculators.Contract;

namespace Provausio.FitnessCalc.Core.Calculators
{
    public class BaiCalculator : IFitnessCalculator
    {
        private readonly BaiRequest _request;

        public BaiCalculator(ICalcRequest request)
        {
            _request = request as BaiRequest;
            if (_request == null)
            {
                throw new ArgumentException(string.Format("The request {0} was invalid for the operation {1}", request.GetType(), this));
            }
        }
        public ICalcResponse Run()
        {
            var result = (_request.HipsCentimeters/ Math.Pow(_request.HeightMeters, 1.5)) - 18;
            return new BaiResponse(result);
        }
    }

    public class BaiRequest
    {
        /// <summary>
        /// Hips in centimeters
        /// </summary>
        public int HipsCentimeters { get; set; }
        /// <summary>
        /// Hips in meters
        /// </summary>
        public double HeightMeters { get; set; }
    }

    public class BaiResponse : CalculatorResponseBase
    {
        public BaiResponse(double result)
        {
            Result = result;
            Operation = OperationTypes.BAI;
            Description = "Body fat percentage";
        }
    }
}
